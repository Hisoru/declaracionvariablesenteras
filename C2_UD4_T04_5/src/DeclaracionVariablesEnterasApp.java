
public class DeclaracionVariablesEnterasApp {

	public static void main(String[] args) {
		
		// Declaración de cuatro variables
		int A = 1;
		int B = 2;
		int C = 3;
		int D = 4;
		
		// Intercambio de variables
		B = C;
		C = A;
		A = D;
		D = B;
		
	}

}
